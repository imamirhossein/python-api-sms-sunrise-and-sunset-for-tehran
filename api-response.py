import requests

def sendmessage(sunrise):
        API_KEY = 'your-api-key'
        url = 'https://api.kavenegar.com/v1/%s/sms/send.json' %API_KEY
        payload = {'receptor':'a-phone-number', 'message':'طلوع آفتاب در ساعت %s می‌باشد'% sunrise}
        result = requests.post(url, data=payload)
        return result

teh_lat = '35.6892'
teh_lng = '51.3890'
url = 'https://api.sunrise-sunset.org/json?lat=%s&lng=%s' % (teh_lat , teh_lng)


response = requests.get(url)

print (sendmessage(response.json()['results']['sunrise']))